package com.minimumy.coroutines_android_1

import android.app.Application
import com.minimumy.coroutines_android_1.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class App: Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>
    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate() {
        super.onCreate()
        instance = this

        DaggerAppComponent.builder()
            .context(this)
            .application(this)
            .build()
            .inject(this)
    }

    companion object{
        lateinit var instance : App
    }
}