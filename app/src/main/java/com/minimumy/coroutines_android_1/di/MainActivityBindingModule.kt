package com.minimumy.coroutines_android_1.di

import android.content.Context
import com.minimumy.coroutines_android_1.MainPresenter
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class MainActivityBindingModule {

    @Provides
    fun provideRootActivityPresenter(context: Context, okHttpClient: OkHttpClient): MainPresenter {
        return MainPresenter(context, okHttpClient)
    }
}