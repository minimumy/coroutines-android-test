package com.minimumy.coroutines_android_1.di

import com.minimumy.coroutines_android_1.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainActivityBindingModule::class])
    abstract fun mainActivity(): MainActivity
}