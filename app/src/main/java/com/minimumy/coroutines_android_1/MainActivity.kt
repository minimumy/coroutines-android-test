package com.minimumy.coroutines_android_1

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import javax.inject.Inject
import javax.inject.Provider
import com.yanzhenjie.permission.AndPermission
import com.yanzhenjie.permission.runtime.Permission


class MainActivity : MvpAppCompatActivity(), MainView, HasAndroidInjector {

    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>
    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    @Inject
    lateinit var presenterProvider: Provider<MainPresenter>

    private val presenter by moxyPresenter { presenterProvider.get() }

    private lateinit var progressFrame: View

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressFrame = findViewById(R.id.progress_frame)

        val startDownloadingButton = findViewById<Button>(R.id.start_downloading_button)
        startDownloadingButton.setOnClickListener {

            AndPermission.with(this)
                .runtime()
                .permission(Permission.Group.STORAGE)
                .onGranted { permissions: List<String?>? ->
                    presenter.onStartDownloadingButtonClick()
                }
                .onDenied{ permissions: List<String?>? ->
                    showToast("Permission required")
                }
                .start()
        }
    }

    override fun showToast(message: String) {
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
    }

    override fun showProgress(show: Boolean) {
        progressFrame.visibility = if (show) View.VISIBLE else View.GONE
    }
}