package com.minimumy.coroutines_android_1

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

interface MainView: MvpView {

    @OneExecution
    fun showToast(message: String)

    @AddToEndSingle
    fun showProgress(show: Boolean)
}