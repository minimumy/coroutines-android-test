package com.minimumy.coroutines_android_1

import android.content.Context
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception

@InjectViewState
class MainPresenter(
    private val context: Context,
    private val okHttpClient: OkHttpClient
): BasePresenter<MainView>() {

    fun onStartDownloadingButtonClick() {

        val fileJob = launchInPresenterScopeLazy(Dispatchers.IO) {
            makeRequest()
        }
        fileJob.start()
    }

    private suspend fun makeRequest() {

        withContext(Dispatchers.Main) {
            viewState.showProgress(true)
        }

        try {

            val request: Request = Request.Builder()
                .url("https://sabnzbd.org/tests/internetspeed/50MB.bin")
                .build()

            val dir = File(context.filesDir, "my")
            if (!dir.exists()) {
                dir.mkdir()
            }

            val responseStream = okHttpClient.newCall(request).execute().body?.byteStream()
            responseStream?.let { inputStream ->

                val f = File(dir,"50MB_${System.currentTimeMillis()}.bin")
                val out = FileOutputStream(f)

                val buffer = ByteArray(1024)
                var len: Int
                while (inputStream.read(buffer).also { len = it } != -1) {
                    out.write(buffer, 0, len)
                }
                out.flush()
                out.close()

                withContext(Dispatchers.Main) {
                    viewState.showProgress(false)
                    viewState.showToast("Download completed!")
                }
            }

        } catch (e: Throwable) {

            Log.e(TAG, "${e.message}")
            viewState.showProgress(false)
            withContext(Dispatchers.Main) {
                viewState.showToast("${e.message}")
            }
        }
    }

    companion object {

        private const val TAG = "MainPresenter"
    }
}