package com.minimumy.coroutines_android_1

import kotlinx.coroutines.*
import moxy.MvpPresenter
import moxy.MvpView

abstract class BasePresenter <V : MvpView> : MvpPresenter<V>() {

    private val mPresenterScope = CoroutineScope(Dispatchers.IO)

    fun launchInPresenterScope(
        coroutineDispatcher: CoroutineDispatcher = Dispatchers.Main,
        callback: suspend () -> Unit
    ): Job {
        return mPresenterScope.launch(coroutineDispatcher) {
            callback.invoke()
        }
    }

    fun launchInPresenterScopeLazy(
        coroutineDispatcher: CoroutineDispatcher = Dispatchers.Main,
        callback: suspend () -> Unit
    ): Job {
        return mPresenterScope.launch(coroutineDispatcher, CoroutineStart.LAZY) {
            callback.invoke()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenterScope.cancel()
    }
}